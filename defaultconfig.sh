# === MANDATORY ===
# set your personal info, uncomment to these lines:
#NAME=""
#SURNAME=""
#NUMBER="0152012345678"  # phone number

# afaik the email needs to be a @fau.de mail
EMAIL="$(tr "[:upper:]" "[:lower:]" <<< "$NAME.$SURNAME@fau.de")"

# this does not change if you dont request a new one
# its the confirmation code for the email
# set it here and never be asked for it again
#MENSACODE="AAAAA"

# === OPTIONAL ===

# provide a default regex to match the mensa name if none is provided in the command line
MENSANAME="Langemarck"

# save the qrcode at the specified location
MENSAQRPATH="/tmp/mensaqr.png"

# if matrix-commander is installed this is the room to which the qr code will be sent
# keep empty to prevent sending
# room adresses look like this: "!ababababababababab:matrix.org"
MATRIXROOM=""
