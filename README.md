# mensaerlnue
A simple shellscript to reserve a seating place for any mensa in Erlangen/Nuernberg.

## Installation
```
makepkg -ic
nvim ~/.config/mensa.sh  # configure your information
```

## Usage
```
mensa 11:30 Langemarck
```
First argument is passed to `date -d` and can therefore be any string describing a time.
Note that the preset timeslots usually start every 10 minutes from `11:00` to `14:00`.

Second argument is passed to `grep -i` to match the mensa name, a default can also be specified in the config file
